import { TestBed, inject } from '@angular/core/testing';

import { BeneficiosService } from './beneficios.service';

describe('BeneficiosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BeneficiosService]
    });
  });

  it('should be created', inject([BeneficiosService], (service: BeneficiosService) => {
    expect(service).toBeTruthy();
  }));
});
