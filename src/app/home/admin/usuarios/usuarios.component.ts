import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { UsuariosService } from "../_services/usuarios.service";
import { AccesosService } from "./../_services/accesos.service";
import { ModulosService } from '../_services/modulos.service';
import { NotificationsService } from 'angular2-notifications';

declare var $: any
import { path } from "../../../config.module";

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {
  userTable:any
  userTypesCombo:any
  foreignCombo:any
  foreignData:any
  selectedData:any
  parentCombo:any
  secondParentCombo:any
  modulos:any = []
  idRol=+localStorage.getItem('currentRolId');
  Agregar = +localStorage.getItem('permisoAgregar')
  Modificar = +localStorage.getItem('permisoModificar')
  Eliminar = +localStorage.getItem('permisoEliminar')
  Mostrar = +localStorage.getItem('permisoMostrar')
  public rowsOnPage = 5;
  public search:any
  Data:any
  private basePath:string = path.path
  dateToday:any
  yearToday:any
  dropdownList = [];
  selectedItem = [];
  selectedItems = [];
  dropdownSettings = [];

  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UsuariosService,
    private secondChildService: ModulosService,
    private childService: AccesosService
  ) { }

  date(dat){
    let date = new Date();
    let beginDate = date.getFullYear()
    this.dateToday = beginDate - (dat.substring(0,4))
  }

  subirImagenes(archivo,form,id){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    var archivos=archivo.srcElement.files;
    let url = `${this.basePath}/api/users/upload/${form.id}`

    var i=0;
    var size=archivos[i].size;
    var type=archivos[i].type;
        if(size<(2*(1024*1024))){
          if(type=="image/png" || type=="image/jpeg" || type=="image/jpg"){
        $("#"+id).upload(url,
            {
              avatar: archivos[i]
          },
          function(respuesta)
          {
            $('#imgAvatar').attr("src",'')
            $('#imgAvatar').attr("src",respuesta.picture)
            $('#Loading').css('display','none')
            $("#"+id).val('')
            $("#barra_de_progreso").val(0)
          },
          function(progreso, valor)
          {

            $("#barra_de_progreso").val(valor);
          }
        );
          }else{
            this.createError("El tipo de imagen no es valido")
            $('#Loading').css('display','none')
          }
      }else{
        this.createError("La imagen es demaciado grande")
        $('#Loading').css('display','none')
      }
  }
  previsualizarImagenes(archivo,tipoAR,id){
    var archivos=archivo.files;
    var i=0;
    var size=archivos[i].size;
    var type=archivos[i].type;
      var target=archivo.value;
      if(size<(2*(1024*1024))){
          if(type=="image/png"){
              if (archivo.files && archivo.files[0]) {
              var reader = new FileReader();
                      reader.onload = function (e) {
                        console.log(e);
                      }
                      reader.readAsDataURL(archivos[i]);
              }
          }else{
              $('#mensajeP2').html('La imagen debe ser de tipo PNG');
              location.href="#mensajeP2";

          }
      }else{
          $('#mensajeP2').html('La imagen es muy pesada, se recomienda subir imagenes de menos de 2MB.');
          location.href="#mensajeP2";
      }
  }
    ngOnInit() {
      let date = new Date();
      this.yearToday = date.getFullYear()+'-'+((date.getMonth()>9)?date.getMonth():'0'+date.getMonth())+'-'+((date.getDay()>9)?date.getDay():'0'+date.getDay())
      this.date(this.yearToday);

      this.cargarUsers()
      this.dropdownList = [
        {"id":1,"itemName":"Mostrar"},
        {"id":2,"itemName":"Agregar"},
        {"id":3,"itemName":"Modificar"},
        {"id":4,"itemName":"Eliminar"}
      ];
      this.userService.getTypes()
                        .then(response => {
                          this.userTypesCombo = response
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    onItemSelect(id:any,event){
      let data:any;
      switch(event.itemName){
        case 'Agregar' : {
          data = {
            usuario : this.selectedData.id,
            modulo : id,
            agregar : 1
          }
          break;
        }
        case 'Modificar' : {
          data = {
            usuario : this.selectedData.id,
            modulo : id,
            modificar : 1
          }
          break;
        }
        case 'Eliminar' : {
          data = {
            usuario : this.selectedData.id,
            modulo : id,
            eliminar : 1
          }
          break;
        }
        case 'Mostrar' : {
          data = {
            usuario : this.selectedData.id,
            modulo : id,
            mostrar : 1
          }
          break;
        }
      }
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.childService.create(data)
                      .then(response => {
                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })
      // console.log(data);

    }
    OnItemDeSelect(id:any,event){
      let data:any;
      switch(event.itemName){
        case 'Agregar' : {
          data = {
            usuario : this.selectedData.id,
            modulo : id,
            agregar : 0
          }
          break;
        }
        case 'Modificar' : {
          data = {
            usuario : this.selectedData.id,
            modulo : id,
            modificar : 0
          }
          break;
        }
        case 'Eliminar' : {
          data = {
            usuario : this.selectedData.id,
            modulo : id,
            eliminar : 0
          }
          break;
        }
        case 'Mostrar' : {
          data = {
            usuario : this.selectedData.id,
            modulo : id,
            mostrar : 0
          }
          break;
        }
      }
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.childService.create(data)
                      .then(response => {
                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })
      // console.log(data);
    }
    onSelectAll(items: any){
    }
    onDeSelectAll(id: any){

    }
    cargarUsers(){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.userService.getAll()
                        .then(response => {
                          this.userTable = response
                          $('#Loading').css('display','none')
                          console.clear
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }

    cargarAccesos(id){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.secondChildService.getAll()
                        .then(response => {
                          this.modulos.length =0;
                          response.forEach(element => {
                            element.accesos = []
                            this.childService.getModuleAccess(id,element.id)
                                              .then(response1 => {
                                                this.selectedItem.length=0;
                                                let cont=1;
                                                if(response1.mostrar=='1'){(element.accesos.push({"id":1,"itemName":"Mostrar"})); cont++}
                                                if(response1.agregar=='1'){(element.accesos.push({"id":2,"itemName":"Agregar"})); cont++}
                                                if(response1.modificar=='1'){(element.accesos.push({"id":3,"itemName":"Modificar"})); cont++}
                                                if(response1.eliminar=='1'){(element.accesos.push({"id":4,"itemName":"Eliminar"})); cont++}
                                                // console.log(element.accesos);
                                                this.dropdownSettings[element.id] = {
                                                  singleSelection: false,
                                                  text: element.nombre,
                                                  selectAllText:'Seleccionar Todos',
                                                  unSelectAllText:'Deseleccionar Todos',
                                                  enableSearchFilter: false,
                                                  classes:"myclass custom-class"
                                                };
                                                this.modulos.push(element)
                                                // console.log(element.accesos);
                                              }).catch(error => {
                                                this.dropdownSettings[element.id] = {
                                                  singleSelection: false,
                                                  text: element.nombre,
                                                  selectAllText:'Seleccionar Todos',
                                                  unSelectAllText:'Deseleccionar Todos',
                                                  enableSearchFilter: false,
                                                  classes:"myclass custom-class"
                                                };
                                                this.modulos.push(element)
                                                $('#Loading').css('display','none')
                                              })


                          });
                          // console.log(response);
                          // console.log(this.modulos);

                          $('#Loading').css('display','none')
                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                          $('#Loading').css('display','none')
                        })
    }

    cargarUser(id:number){
      this.userService.getSingle(id)
                        .then(response => {
                          this.selectedData = response;
                          this.cargarAccesos(id)
                          let now = response.birthday;
                          this.date(now)
                          // console.log(response);

                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    updateUser(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      let tutor:any = null
      let teacher:any = null
      let student:any = null
      switch (formValue.type) {
        case '1':{
          student=this.selectedData.foreign*1
          break;
        }
        case '2':{
          teacher=this.selectedData.foreign*1
          break;
        }
        case '3':{
          tutor=this.selectedData.foreign*1
          break;
        }
        case 1:{
          student=this.selectedData.foreign*1
          break;
        }
        case 2:{
          teacher=this.selectedData.foreign*1
          break;
        }
        case 3:{
          tutor=this.selectedData.foreign*1
          break;
        }
        default:{
          break;
        }
      }


      let data = {
        id: formValue.id,
        username: formValue.username,
        email: formValue.email,
        firstname: formValue.firstname?formValue.firstname:'',
        lastname: formValue.lastname?formValue.lastname:'',
        type: formValue.type,
        student: student,
        teacher: teacher,
        youtube_channel: formValue.youtube_channel,
        tutor: tutor
      }
      //console.log(data)
      this.userService.update(data)
                        .then(response => {
                          this.cargarUsers()
                          console.clear
                          $("#editModal .close").click();
                          this.create('Usuario Actualizado exitosamente')
                          $('#Loading').css('display','none')

                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

    }
    deleteUser(id:string){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      if(confirm("¿Desea Eliminar el USuario?")){
      this.userService.delete(id)
                        .then(response => {
                          this.cargarUsers()
                          console.clear
                          this.create('Usuario Eliminado exitosamente')
                          $('#Loading').css('display','none')

                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
      }else{
        $('#Loading').css('display','none')
      }

    }
    insertUser(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')

      let pass = this.generar(25)
      let data = {
        username: formValue.username,
        email: formValue.email,
        firstname: formValue.firstname,
        lastname: formValue.lastname,
        type: formValue.type,
        password: pass,
        work: formValue.work,
        description: formValue.description,
        age: formValue.age,
        phone: formValue.phone,
        birthday: this.yearToday,
        youtube_channel: formValue.youtube_channel
      }
      // console.log(data)
      this.userService.create(data)
                        .then(response => {
                          this.cargarUsers()
                          console.clear
                          this.create('Usuario Ingresado')
                          $("#insertModal .close").click();
                          $('#Loading').css('display','none')
                          $('#insert-form')[0].reset()

                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })


    }
    generar(longitud)
    {
      let i:number
      var caracteres = "123456789+/-*abcdefghijkmnpqrtuvwxyz123456789+/-*ABCDEFGHIJKLMNPQRTUVWXYZ12346789+/-*";
      var contraseña = "";
      for (i=0; i<longitud; i++) contraseña += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
      return contraseña;
    }
    comboTutor(id:string){
      this.userService.getTutor(id)
                        .then(response => {
                          this.Data = {
                            lastnameData:response.lastname,
                            firstnameData:response.firstname
                          }
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    comboTeacher(id:string){
      this.userService.getTeacher(id)
                        .then(response => {
                          this.Data = {
                            lastnameData:response.lastname,
                            firstnameData:response.firstname
                          }
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    comboStudent(id:string){
      this.userService.getStudent(id)
                        .then(response => {
                          this.Data = {
                            lastnameData:response.lastname,
                            firstnameData:response.firstname
                          }
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    getForeign(id:string,type:string){
      //console.log(`${id} ${type}`)
      switch (type) {
        case '1':{
          this.comboStudent(id)
          break;
        }
        case '2':{
          this.comboTeacher(id)
          break;
        }
        case '3':{
          this.comboTutor(id)
          break;
        }
        default:{
          console.log(`${id} id
          ${type} tipo`)
          break;
        }
      }
    }
    comboTutors(type:string){
      this.userService.getTutors()
                        .then(response => {
                          this.foreignCombo = response
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    comboTeachers(type:string){
      this.userService.getTeachers()
                        .then(response => {
                          this.foreignCombo = response
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    comboStudents(type:string){
      this.userService.getStudents()
                        .then(response => {
                          this.foreignCombo = response
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    cargarForanea(type:string){

      switch (type) {
        case '1':{
          this.comboStudents(type)
          this.foreignData = {
            title:'Alumnos',
            type: type
          };
          break;
        }
        case '2':{
          this.comboTeachers(type)
          this.foreignData = {
            title:'Maestros',
            type: type
          };
          break;
        }
        case '3':{
          this.comboTutors(type)
          this.foreignData = {
            title:'Tutores',
            type: type
          };
          break;
        }
        case '4':{
          this.foreignData = {
            title:'',
            type: type
          };
          break;
        }
        default:{
          this.foreignData = {
            title:'',
            type: type
          };
          console.log(`combo foraneo no encontrado ${type} tipo`)
          break;
        }
      }
    }
  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }
}
