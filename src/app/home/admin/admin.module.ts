import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { DataTableModule } from "angular2-datatable";
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';


import { AdminRoutingModule } from './admin.routing';
import { AdminComponent } from './admin.component';
import { LoaderComponent } from './loader/loader.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';

import { UsuariosService } from "./_services/usuarios.service";
import { UsersTypesService } from "./_services/users-types.service";
import { EventsService } from "./_services/events.service";
import { ProductsService } from "./_services/products.service";
import { CategorysService } from "./_services/categorys.service";
import { OrdersService } from "./_services/orders.service";
import { ModulosService } from "./_services/modulos.service";
import { AccesosService } from "./_services/accesos.service";

import { SimpleNotificationsModule } from 'angular2-notifications';
import { ChartsModule } from 'ng2-charts';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2DragDropModule } from 'ng2-drag-drop';
import { LoadersCssModule } from 'angular2-loaders-css';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { EventosComponent } from './eventos/eventos.component';
import { UsuariosTipoComponent } from './usuarios-tipo/usuarios-tipo.component';
import { UsuariosComponent } from './usuarios/usuarios.component';

import { Ng2MapModule} from 'ng2-map';
import { ProductosComponent } from './productos/productos.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { ClientesComponent } from './clientes/clientes.component';
import { UsuariosPendientesComponent } from './usuarios-pendientes/usuarios-pendientes.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ChartsModule,
    SimpleNotificationsModule.forRoot(),
    Ng2SearchPipeModule,
    Ng2DragDropModule.forRoot(),
    Ng2MapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyCdJAwErIy3KmcE_EfHACIvL0Nl1RjhcUo'}),
    LoadersCssModule,
    AngularMultiSelectModule,
    AdminRoutingModule
  ],
  declarations: [
    AdminComponent,
    LoaderComponent,
    DashboardComponent,
    ProfileComponent,
    ConfiguracionComponent,
    EventosComponent,
    UsuariosTipoComponent,
    UsuariosComponent,
    ProductosComponent,
    CategoriasComponent,
    PedidosComponent,
    ClientesComponent,
    UsuariosPendientesComponent
  ],
  providers: [
    UsuariosService,
    UsersTypesService,
    EventsService,
    ModulosService,
    AccesosService,
    ProductsService,
    CategorysService,
    OrdersService
  ]
})
export class AdminModule { }
