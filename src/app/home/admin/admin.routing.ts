import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from "./admin.component";
import { DashboardComponent } from "./dashboard/dashboard.component";


import { ProfileComponent } from "./profile/profile.component";
import { ConfiguracionComponent } from "./configuracion/configuracion.component";
import { UsuariosComponent } from "./usuarios/usuarios.component";
import { UsuariosTipoComponent } from "./usuarios-tipo/usuarios-tipo.component";
import { EventosComponent } from "./eventos/eventos.component";
import { ProductosComponent } from "./productos/productos.component";
import { CategoriasComponent } from "./categorias/categorias.component";
import { PedidosComponent } from "./pedidos/pedidos.component";
import { ClientesComponent } from "./clientes/clientes.component";
import { UsuariosPendientesComponent } from "./usuarios-pendientes/usuarios-pendientes.component";

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: AdminComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'config', component: ConfiguracionComponent },
    { path: 'usuarios', component: UsuariosComponent },
    { path: 'usuarios-inactivos', component: UsuariosPendientesComponent },
    { path: 'clientes', component: ClientesComponent },
    { path: 'usuarios-tipo', component: UsuariosTipoComponent },
    { path: 'eventos', component: EventosComponent },
    { path: 'productos', component: ProductosComponent },
    { path: 'categorias', component: CategoriasComponent },
    { path: 'pedidos', component: PedidosComponent },
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
