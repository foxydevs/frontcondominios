import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpresaRoutingModule } from './empresa.routing';
import { EmpresaComponent } from './empresa.component';
import { LoaderComponent } from './loader/loader.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
  imports: [
    CommonModule,
    EmpresaRoutingModule
  ],
  declarations: [EmpresaComponent, LoaderComponent, DashboardComponent, ProfileComponent]
})
export class EmpresaModule { }
