import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { OrdersService } from "./../../admin/_services/orders.service";
import { UsuariosService } from "./../../admin/_services/usuarios.service";
import { ProductsService } from "./../../admin/_services/products.service";
import { NotificationsService } from 'angular2-notifications';
declare var $: any
import { path } from "../../../config.module";

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit {
  Table:any
  comboParent:any
  comboChild:any
  selectedData:any
  parentCombo:any
  beginDate:any
  endDate:any
  userId:any = localStorage.getItem('currentId');
  public rowsOnPage = 5;
  public search:any
  private basePath:string = path.path
  positions:any
  lat:any
  lng:any
  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private mainService: OrdersService,
    private parentService: UsuariosService,
    private childService: ProductsService,
  ) { }

    ngOnInit() {
      let date = new Date();
      let month = date.getMonth()+1;
      let month2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month
      }
      this.beginDate= date.getFullYear()+'-'+month2+'-01'
      month = date.getMonth()+2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month;
      }
      this.endDate=((date.getHours()<10)?'0'+date.getHours():date.getHours())+':'+((date.getMinutes()<10)?'0'+date.getMinutes():date.getMinutes())+':'+((date.getSeconds()<10)?'0'+date.getSeconds():date.getSeconds())
      this.cargarAll()
      this.cargarCombo()
      this.cargarComboChild()
    }
    cargarAll(){
      this.mainService.getAll(this.userId)
                        .then(response => {
                          this.Table = response
                          // console.log(response);

                          $("#editModal .close").click();
                          $("#insertModal .close").click();
                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }

    cargarCombo(){
      this.parentService.getAll()
                        .then(response => {
                          this.comboParent = response
                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    cargarComboChild(){
      this.childService.getAll(this.userId)
                        .then(response => {
                          this.comboChild = response
                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }

    onMapReady(map) {

    }
    onIdle(event) {
      // console.log('map', event.target);
    }
    onMarkerInit(marker) {
      // console.log('marker', marker);
    }
    onMapClick(event) {
      this.positions = event.latLng;
      let positions1 = event.latLng + '';
      let pos = positions1.replace(')','').replace('(','').split(',')
      this.lat = pos[0]
      this.lng = pos[1]
      event.target.panTo(this.positions);
      // console.log(this.lat+' @ '+this.lng+' @ '+event.latLng+'\n'+pos[0]);

    }
    subirImagenes(archivo,form,id){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      var archivos=archivo.srcElement.files;
      let url = `${this.basePath}/api/product/upload/${form.id}`

      var i=0;
      var size=archivos[i].size;
      var type=archivos[i].type;
          if(size<(2*(1024*1024))){
            if(type=="image/png" || type=="image/jpeg" || type=="image/jpg"){
          $("#"+id).upload(url,
              {
                avatar: archivos[i]
            },
            function(respuesta)
            {
              $('#imgAvatar').attr("src",'')
              $('#imgAvatar').attr("src",respuesta.picture)
              $('#Loading').css('display','none')
              $("#"+id).val('')
              $("#barra_de_progreso").val(0)
            },
            function(progreso, valor)
            {

              $("#barra_de_progreso").val(valor);
            }
          );
            }else{
              this.createError("El tipo de imagen no es valido")
              $('#Loading').css('display','none')
            }
        }else{
          this.createError("La imagen es demaciado grande")
          $('#Loading').css('display','none')
        }
    }
    previsualizarImagenes(archivo,tipoAR,id){
      var archivos=archivo.files;
      var i=0;
      var size=archivos[i].size;
      var type=archivos[i].type;
        var target=archivo.value;
        if(size<(2*(1024*1024))){
            if(type=="image/png"){
                if (archivo.files && archivo.files[0]) {
                var reader = new FileReader();
                        reader.onload = function (e) {
                          console.log(e);
                        }
                        reader.readAsDataURL(archivos[i]);
                }
            }else{
                $('#mensajeP2').html('La imagen debe ser de tipo PNG');
                location.href="#mensajeP2";

            }
        }else{
            $('#mensajeP2').html('La imagen es muy pesada, se recomienda subir imagenes de menos de 2MB.');
            location.href="#mensajeP2";
        }
    }

    cargarSingle(id:number){
      this.mainService.getSingle(id)
                        .then(response => {
                          this.selectedData = response;
                          this.positions = '('+response.latitude+', '+response.longitude+')'
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    update(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      //console.log(data)
      this.mainService.update(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Evento Actualizado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })

    }
    delete(id:string){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Evento Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })

    }
    insert(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.create(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Evento Ingresado')
                          $('#Loading').css('display','none')
                          $('#insert-form')[0].reset()

                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })


    }

  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }
}
