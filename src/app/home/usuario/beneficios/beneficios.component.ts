import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { BeneficiosService } from "../../admin/_services/beneficios.service";
import { CategorysService } from "../../admin/_services/categorys.service";
import { NotificationsService } from 'angular2-notifications';
declare var $: any
import { path } from "../../../config.module";

@Component({
  selector: 'app-beneficios',
  templateUrl: './beneficios.component.html',
  styleUrls: ['./beneficios.component.css']
})
export class BeneficiosComponent implements OnInit {
  Table:any
  comboChild:any
  selectedData:any
  parentCombo:any
  beginDate:any
  endDate:any
  userId:any = localStorage.getItem('currentId');
  public rowsOnPage = 5;
  public search:any
  private basePath:string = path.path
  positions:any
  lat:any
  lng:any
  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private mainService: BeneficiosService,
    private childService: CategorysService,
  ) { }

    ngOnInit() {
      this.cargarAll()
    }
    darValor(){
      this.userId = localStorage.getItem('currentId');
    }
    cargarAll(){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.userId = localStorage.getItem('currentId');
      this.mainService.getAll(this.userId)
                        .then(response => {
                          this.Table = response
                          $('#Loading').css('display','none')
                          console.clear
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    subirImagenes(archivo,form,id){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      var archivos=archivo.srcElement.files;
      let url = `${this.basePath}/api/product/upload/${form.id}`

      var i=0;
      var size=archivos[i].size;
      var type=archivos[i].type;
          if(size<(2*(1024*1024))){
            if(type=="image/png" || type=="image/jpeg" || type=="image/jpg"){
          $("#"+id).upload(url,
              {
                avatar: archivos[i]
            },
            function(respuesta)
            {
              $('#imgAvatar').attr("src",'')
              $('#imgAvatar').attr("src",respuesta.picture)
              $('#Loading').css('display','none')
              $("#"+id).val('')
              $("#barra_de_progreso").val(0)
            },
            function(progreso, valor)
            {

              $("#barra_de_progreso").val(valor);
            }
          );
            }else{
              this.createError("El tipo de imagen no es valido")
              $('#Loading').css('display','none')
            }
        }else{
          this.createError("La imagen es demaciado grande")
          $('#Loading').css('display','none')
        }
    }

    cargarSingle(id:number){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.getSingle(id)
                        .then(response => {
                          this.selectedData = response;
                          this.selectedData.name = response.title;
                          console.log(response);

                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    update(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      //console.log(data)
      formValue.title = formValue.name;
      this.mainService.update(formValue)
                        .then(response => {
                          console.clear
                          this.create('Producto Actualizado exitosamente')
                          $("#editModal .close").click();
                          $('#Loading').css('display','none')
                          this.cargarAll()
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

    }
    delete(id:string){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      if(confirm("¿Desea Eliminar el producto?")){
        this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Producto Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

      }else{
        $('#Loading').css('display','none')
      }

    }
    insert(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      formValue.title = formValue.name;
      this.mainService.create(formValue)
                        .then(response => {
                          console.clear
                          this.create('Producto Ingresado')
                          $('#Loading').css('display','none')
                          $('#insert-form #name').val('');
                          $('#insert-form #description').val('');
                          $('#insert-form #price').val('');
                          $('#insert-form #category').val('');
                          $('#Loading').css('display','none')
                          this.cargarAll()
                          $("#insertModal .close").click();
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })


    }

  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }
}
