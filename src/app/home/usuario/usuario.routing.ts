import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioComponent } from "./usuario.component";
import { DashboardComponent } from "./dashboard/dashboard.component";


import { ProfileComponent } from "./profile/profile.component";
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { EventosComponent } from './eventos/eventos.component';
import { ProductosComponent } from './productos/productos.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { TipsComponent } from './tips/tips.component';
import { BeneficiosComponent } from './beneficios/beneficios.component';
import { ComerciosComponent } from './comercios/comercios.component';
import { MotivacionComponent } from './motivacion/motivacion.component';
import { RankingComponent } from './ranking/ranking.component';
import { RetosComponent } from './retos/retos.component';
import { SocialComponent } from './social/social.component';
import { WorkoutComponent } from './workout/workout.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: UsuarioComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'config', component: ConfiguracionComponent },
    { path: 'eventos', component: EventosComponent },
    { path: 'productos', component: ProductosComponent },
    { path: 'categorias', component: CategoriasComponent },
    { path: 'tips', component: TipsComponent },
    { path: 'pedidos', component: PedidosComponent },
    { path: 'beneficios', component: BeneficiosComponent },
    { path: 'comercios', component: ComerciosComponent },
    { path: 'workout', component: WorkoutComponent },
    { path: 'retos', component: RetosComponent },
    { path: 'social', component: SocialComponent },
    { path: 'ranking', component: RankingComponent },
    { path: 'motivacion', component: MotivacionComponent },
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
