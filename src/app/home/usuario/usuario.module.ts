import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { DataTableModule } from "angular2-datatable";


import { UsuarioRoutingModule } from './usuario.routing';
import { UsuarioComponent } from './usuario.component';
import { LoaderComponent } from './loader/loader.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';

import { EventsService } from './../admin/_services/events.service';
import { CategorysService } from './../admin/_services/categorys.service';
import { ProductsService } from './../admin/_services/products.service';
import { OrdersService } from './../admin/_services/orders.service';
import { UsuariosService } from "./../admin/_services/usuarios.service";
import { TipsService } from "./../admin/_services/tips.service";
import { BeneficiosService } from "./../admin/_services/beneficios.service";
import { ComerciosService } from "./../admin/_services/comercios.service";
import { RetosService } from "./../admin/_services/retos.service";
import { UsuariosRetosService } from "./../admin/_services/usuarios-retos.service";
import { WorkoutService } from "./../admin/_services/workout.service";
import { MotivacionService } from "./../admin/_services/motivacion.service";

import { SimpleNotificationsModule } from 'angular2-notifications';
import { ChartsModule } from 'ng2-charts';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2DragDropModule } from 'ng2-drag-drop';
import { LoadersCssModule } from 'angular2-loaders-css';
import { Ng2MapModule} from 'ng2-map';

import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { ProductosComponent } from './productos/productos.component';
import { EventosComponent } from './eventos/eventos.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { TipsComponent } from './tips/tips.component';
import { BeneficiosComponent } from './beneficios/beneficios.component';
import { ComerciosComponent } from './comercios/comercios.component';
import { WorkoutComponent } from './workout/workout.component';
import { RetosComponent } from './retos/retos.component';
import { RankingComponent } from './ranking/ranking.component';
import { SocialComponent } from './social/social.component';
import { MotivacionComponent } from './motivacion/motivacion.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ChartsModule,
    SimpleNotificationsModule.forRoot(),
    Ng2SearchPipeModule,
    Ng2DragDropModule.forRoot(),
    Ng2MapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyCdJAwErIy3KmcE_EfHACIvL0Nl1RjhcUo'}),
    LoadersCssModule,
    UsuarioRoutingModule
  ],
  declarations: [
    UsuarioComponent,
    LoaderComponent,
    DashboardComponent,
    ProfileComponent,
    ConfiguracionComponent,
    ProductosComponent,
    EventosComponent,
    CategoriasComponent,
    PedidosComponent,
    TipsComponent,
    BeneficiosComponent,
    ComerciosComponent,
    WorkoutComponent,
    RetosComponent,
    RankingComponent,
    SocialComponent,
    MotivacionComponent
  ],
  providers: [
    UsuariosService,
    EventsService,
    CategorysService,
    ProductsService,
    OrdersService,
    BeneficiosService,
    ComerciosService,
    RetosService,
    UsuariosRetosService,
    WorkoutService,
    MotivacionService,
    TipsService
  ]
})
export class UsuarioModule { }
