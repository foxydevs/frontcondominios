import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { EventsService } from "../../admin/_services/events.service";
import { NotificationsService } from 'angular2-notifications';
declare var $: any
import { path } from "../../../config.module";

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})
export class EventosComponent implements OnInit {
  Table:any
  comboParent:any
  selectedData:any
  userId:any = localStorage.getItem('currentId');
  beginDate:any
  endDate:any
  public rowsOnPage = 5;
  public search:any
  private basePath:string = path.path
  positions:any
  lat:any = 14.66530813990437
  lng:any = -90.51709514672852
  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private mainService: EventsService,
  ) { }

    ngOnInit() {
      let date = new Date();
      let month = date.getMonth()+1;
      let month2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month
      }
      this.beginDate= date.getFullYear()+'-'+month2+'-01'
      month = date.getMonth()+2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month;
      }
      this.endDate=((date.getHours()<10)?'0'+date.getHours():date.getHours())+':'+((date.getMinutes()<10)?'0'+date.getMinutes():date.getMinutes())+':'+((date.getSeconds()<10)?'0'+date.getSeconds():date.getSeconds())
      this.cargarAll()
    }
    darValor(){
      this.userId = localStorage.getItem('currentId');
    }
    cargarAll(){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.userId = localStorage.getItem('currentId');
      this.mainService.getAll(this.userId)
                        .then(response => {
                          this.Table = response
                          $('#Loading').css('display','none')
                          console.clear
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }

    onMapReady(map) {

    }
    onIdle(event) {
      // console.log('map', event.target);
    }
    onMarkerInit(marker) {
      // console.log('marker', marker);
    }
    onMapClick(event) {
      this.positions = event.latLng;
      let positions1 = event.latLng + '';
      let pos = positions1.replace(')','').replace('(','').split(',')
      this.lat = pos[0].replace(' ','')
      this.lng = pos[1].replace(' ','')
      event.target.panTo(this.positions);
      this.positions = new google.maps.LatLng(parseFloat(this.lat), parseFloat(this.lng));

      // console.log(this.lat+' @ '+this.lng+' @ '+event.latLng+'\n'+this.positions);
    }
    subirImagenes(archivo,form,id){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      var archivos=archivo.srcElement.files;
      let url = `${this.basePath}/api/event/upload/${form.id}`

      var i=0;
      var size=archivos[i].size;
      var type=archivos[i].type;
          if(size<(2*(1024*1024))){
            if(type=="image/png" || type=="image/jpeg" || type=="image/jpg"){
          $("#"+id).upload(url,
              {
                avatar: archivos[i]
            },
            function(respuesta)
            {
              $('#imgAvatar').attr("src",'')
              $('#imgAvatar').attr("src",respuesta.picture)
              $('#Loading').css('display','none')
              $("#"+id).val('')
              $("#barra_de_progreso").val(0)
            },
            function(progreso, valor)
            {

              $("#barra_de_progreso").val(valor);
            }
          );
            }else{
              this.createError("El tipo de imagen no es valido")
              $('#Loading').css('display','none')
            }
        }else{
          this.createError("La imagen es demaciado grande")
          $('#Loading').css('display','none')
        }
    }

    cargarSingle(id:number){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.getSingle(id)
                        .then(response => {
                          this.selectedData = response;
                          this.positions = new google.maps.LatLng(parseFloat(response.latitude), parseFloat(response.longitude));
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    update(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      formValue.latitude = this.lat
      formValue.longitude = this.lng
      //console.log(data)
      this.mainService.update(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Evento Actualizado exitosamente')
                          $("#editModal .close").click();
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

    }
    delete(id:string){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      if(confirm("¿Desea Eliminar el evento?")){
        this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Evento Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
      }else{
        $('#Loading').css('display','none')
      }

    }
    insert(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.create(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Evento Ingresado')
                          $('#Loading').css('display','none')
                          $('#insert-form #address').val('');
                          $('#insert-form #description').val('');
                          $('#insert-form #date').val(this.beginDate);
                          $('#insert-form #time').val(this.endDate);
                          $("#insertModal .close").click();
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })


    }

  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }
}
