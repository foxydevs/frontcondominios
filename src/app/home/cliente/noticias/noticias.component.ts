import { Component,Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params } from "@angular/router";
import { Location } from '@angular/common';

import { ProductsService } from "../../admin/_services/products.service";
import { UsuariosService } from "../../admin/_services/usuarios.service";
import { CategorysService } from "../../admin/_services/categorys.service";
import { OrdersService } from "../../admin/_services/orders.service";
import { NotificationsService } from 'angular2-notifications';
declare var $: any
import { path } from "../../../config.module";

import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css']
})
export class NoticiasComponent implements OnInit {
  @Input() token:any;
  @Input() ERN:any;
  Table:any
  comboParent:any
  comboChild:any
  selectedData:any
  parentCombo:any
  beginDate:any
  endDate:any
  userId:any;
  id=localStorage.getItem('currentId');
  public rowsOnPage = 5;
  public search:any
  private basePath:string = path.path
  positions:any
  lat:any
  lng:any
  tokenID:any
  ERNID:any
  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private mainService: ProductsService,
    private parentService: UsuariosService,
    private childService: CategorysService,
    private plusService: OrdersService,
  ) { }

    ngOnInit() {

        this.route.params.subscribe(params => {
          if(params['token']!=null){
              this.tokenID = +params['token'];
          }
          if(params['ERN']!=null){
              this.ERNID = params['ERN'];
          }
        });


      let date = new Date();
      let month = date.getMonth()+1;
      let month2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month
      }
      this.beginDate= date.getFullYear()+'-'+month2+'-01'
      month = date.getMonth()+2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month;
      }
      this.endDate=((date.getHours()<10)?'0'+date.getHours():date.getHours())+':'+((date.getMinutes()<10)?'0'+date.getMinutes():date.getMinutes())+':'+((date.getSeconds()<10)?'0'+date.getSeconds():date.getSeconds())
      if(this.tokenID){
        this.userId=this.tokenID
        this.cargarAll()
      }
      if(this.ERNID){
        $('#editModal').modal()
        this.cargarSingle(this.ERNID)
      }

      this.cargarCombo()
      this.cargarComboChild()
    }
    cargarAll(){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.getAll(this.userId)
                        .then(response => {
                          this.Table = response
                          // $("#editModal .close").click();
                          $("#insertModal .close").click();
                          $('#Loading').css('display','none')
                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }

    cargarCombo(){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.parentService.getAllMine(+this.id)
                        .then(response => {
                          this.comboParent = response
                          $('#Loading').css('display','none')
                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    cargarComboChild(){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.childService.getAll(this.userId)
                        .then(response => {
                          this.comboChild = response
                          $('#Loading').css('display','none')
                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }

    onMapReady(map) {

    }
    onIdle(event) {
      // console.log('map', event.target);
    }
    onMarkerInit(marker) {
      // console.log('marker', marker);
    }
    onMapClick(event) {
      this.positions = event.latLng;
      let positions1 = event.latLng + '';
      let pos = positions1.replace(')','').replace('(','').split(',')
      this.lat = pos[0]
      this.lng = pos[1]
      event.target.panTo(this.positions);
      // console.log(this.lat+' @ '+this.lng+' @ '+event.latLng+'\n'+pos[0]);

    }
    subirImagenes(archivo,form,id){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      var archivos=archivo.srcElement.files;
      let url = `${this.basePath}/api/product/upload/${form.id}`

      var i=0;
      var size=archivos[i].size;
      var type=archivos[i].type;
          if(size<(2*(1024*1024))){
            if(type=="image/png" || type=="image/jpeg" || type=="image/jpg"){
          $("#"+id).upload(url,
              {
                avatar: archivos[i]
            },
            function(respuesta)
            {
              $('#imgAvatar').attr("src",'')
              $('#imgAvatar').attr("src",respuesta.picture)
              $('#Loading').css('display','none')
              $("#"+id).val('')
              $("#barra_de_progreso").val(0)
            },
            function(progreso, valor)
            {

              $("#barra_de_progreso").val(valor);
            }
          );
            }else{
              this.createError("El tipo de imagen no es valido")
              $('#Loading').css('display','none')
            }
        }else{
          this.createError("La imagen es demaciado grande")
          $('#Loading').css('display','none')
        }
    }
    previsualizarImagenes(archivo,tipoAR,id){
      var archivos=archivo.files;
      var i=0;
      var size=archivos[i].size;
      var type=archivos[i].type;
        var target=archivo.value;
        if(size<(2*(1024*1024))){
            if(type=="image/png"){
                if (archivo.files && archivo.files[0]) {
                var reader = new FileReader();
                        reader.onload = function (e) {
                          console.log(e);
                        }
                        reader.readAsDataURL(archivos[i]);
                }
            }else{
                $('#mensajeP2').html('La imagen debe ser de tipo PNG');
                location.href="#mensajeP2";

            }
        }else{
            $('#mensajeP2').html('La imagen es muy pesada, se recomienda subir imagenes de menos de 2MB.');
            location.href="#mensajeP2";
        }
    }

    cargarSingle(id:number){
      this.mainService.getSingle(id)
                        .then(response => {
                          this.selectedData = response;
                          this.positions = '('+response.latitude+', '+response.longitude+')'
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    update(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      let ernn =this.generar(4);
      let orden = {
        cantidad:parseFloat(formValue.quantity),
        descripcion:this.selectedData.name.replace(' ','').replace(' ','').replace(' ','').replace(' ','').replace(' ','').replace(' ','')+"",
        precio:parseFloat(this.selectedData.price),
        id:this.selectedData.id+"",
        url:"http://me.gtechnology.gt",
        ern:ernn+""
      }
      let data = {
        comment   :formValue.comment,
        unit_price:this.selectedData.price,
        quantity  :formValue.quantity,
        product   :this.selectedData.id,
        user      :this.id,
        ern       : ernn
      }
      console.log(orden)
      this.plusService.pay(orden)
                        .then(response2 => {
                          this.plusService.create(data)
                                            .then(response => {
                                              location.href = response2.token
                                              }).catch(error => {
                                                console.clear
                                                $('#Loading').css('display','none')
                                                this.createError(error)
                                              })
                          this.cargarAll()
                          console.clear
                          this.create('Evento Actualizado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })


    }
    delete(id:string){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Evento Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

    }
    generar(longitud)
    {
      let i:number
      var caracteres = "12345678912346789";
      var contraseña = "";
      for (i=0; i<longitud; i++) contraseña += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
      return contraseña;
    }
    insert(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.create(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Evento Ingresado')
                          $('#Loading').css('display','none')
                          $('#insert-form')[0].reset()

                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })


    }

  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }
}
