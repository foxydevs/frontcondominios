import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClienteComponent } from "./cliente.component";
import { DashboardComponent } from "./dashboard/dashboard.component";

import { ProfileComponent } from "./profile/profile.component";
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { EventosComponent } from './eventos/eventos.component';
import { ProductosComponent } from './productos/productos.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { ReciboComponent } from './recibo/recibo.component';
import { NoticiasComponent } from './noticias/noticias.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: ClienteComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'config', component: ConfiguracionComponent },
    { path: 'eventos', component: EventosComponent },
    { path: 'productos/:token', component: ProductosComponent },
    { path: 'productos/:token/:ERN', component: ProductosComponent },
    { path: 'productos', component: ProductosComponent },
    { path: 'categorias', component: CategoriasComponent },
    { path: 'pedidos', component: PedidosComponent },
    { path: 'noticias', component: NoticiasComponent },
    { path: 'pedidos/:token/:ERN', component: ReciboComponent },
    { path: 'pedidos/:token', component: ReciboComponent },
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteRoutingModule { }
